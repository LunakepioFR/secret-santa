import Countdown from "./Countdown";

function Home(props) {
  return (
    <div className="container">
      <div className="title">
        <h1>
          Make someone's <span>Christmas 🎅🏻</span>
        </h1>
        <button onClick={() => props.setIsHome(false)} href="#">
          Make your elves list here
        </button>
      </div>
      <Countdown />
    </div>
  );
}

export default Home;
