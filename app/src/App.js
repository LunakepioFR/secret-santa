import * as React from 'react';
import "./assets/styles/App.scss";
import ssanta from "./assets/ssanta.png";
import Home from "./Home";
import Elves from './Elves';

function App() {
  const [isHome, setIsHome] = React.useState(false);


  return (
    <div className="App">
      <div className="top">
        <header>
          <img src={ssanta} className="App-logo" alt="logo" width="200px" />
        </header>
        {isHome ? <Home setIsHome={setIsHome}/> : <Elves/>}
      </div>


    </div>
  );
}

export default App;
