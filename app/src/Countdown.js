import * as React from "react";
import "./assets/styles/App.scss";

function Countdown() {
  let countDownDate = new Date("Dec 25, 2022 00:00:00").getTime();

  const [days, setDays] = React.useState(0);
  const [hours, setHours] = React.useState(0);
  const [minutes, setMinutes] = React.useState(0);
  const [seconds, setSeconds] = React.useState(0);

  React.useEffect(() => {
    const interval = setInterval(() => {
      let now = new Date().getTime();
      let distance = countDownDate - now;

      setDays(Math.abs(Math.floor(distance / (1000 * 60 * 60 * 24))));
      setHours(
        Math.abs(
          Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
        )
      );
      setMinutes(
        Math.abs(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)))
      );
      setSeconds(Math.abs(Math.floor((distance % (1000 * 60)) / 1000)));
    }, 0);
    return () => clearInterval(interval);
  }, [countDownDate]);

  return (
    <div className="countdown">
      <div className="countdown-container">
        <div id="days" className="time">
          <p>{days}</p>
          <p>days</p>
        </div>

        <div id="hours" className="time">
          <p>{hours}</p>
          <p>hours</p>
        </div>

        <div id="minutes" className="time">
          <p>{minutes}</p>
          <p>minutes</p>
        </div>

        <div id="seconds" className="time">
          <p>{seconds}</p>
          <p>seconds</p>
        </div>
      </div>

    </div>
  );
}

export default Countdown;
