import axios from "axios";
import * as React from "react";
import Elve from "./Elve";
function Elves() {
  const [elves, setElves] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [error, setError] = React.useState(null);

  const [name, setName] = React.useState("");
  const [mail, setMail] = React.useState("");

  const getAllElves = async () => {
    const response = await axios.get("http://localhost:8080/getAll");
    return response.data;
  };

  React.useEffect(() => {
    getAllElves()
      .then((data) => {
        setElves(data);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
  }, []);

  if (loading) return "Loading...";
  if (error) return "Error!";

  async function createElve(name, mail) {
    if (name === "" && mail === ""){
      alert("Please fill in all the fields");
      return;
    }
    else {
      //if mail doesn't contain @, alert
      if (!mail.includes("@")){
        alert("Please enter a valid email");
        return;
      } else {
        const response = await axios.post("http://localhost:8080/createOne", {
          name: name,
          mail: mail,
        });
        setName("");
        setMail("");
        const length = [...elves];
        //get last id from ...elves
        const lastId = length[length.length - 1].id;
        const newElve = {
          id: lastId + 1,
          name: name,
          mail: mail,
        };
        setElves([...elves, newElve]);
        return response.data;
      }
    }
  }

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      createElve(name, mail);
    }
  };

  window.addEventListener("keydown", handleKeyDown);
  return (
    <div className="listContainer">
      <div className="addEntry">
        <div className="text">
          <p>Add an elve ?</p>
          <span>The more the merrier</span>
        </div>
        <div className="interract">
          <div className="add">
            <input
              className="entry"
              type="text"
              placeholder="Name and Surname"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <input
              className="entry"
              type="email"
              placeholder="Mail address"
              value={mail}
              onChange={(e) => setMail(e.target.value)}
            />
            <button
              className="entry"
              type="submit"
              onClick={() => createElve(name, mail)}
            >
              Add
            </button>
          </div>
        </div>
      </div>
      <h2>🎅🏻 Santa's team :</h2>
      <div className="list" id="list">
        {elves.map((element) => (
          <Elve name={element.name} mail={element.mail} id={element.id} />
        ))}
      </div>
    </div>
  );
}

export default Elves;
