import axios from "axios";
import * as React from "react";
function Elve(props) {
  async function del(id) {
    const response = await axios.delete(
      `http://localhost:8080/deleteOne/${id}`
    );
    //redirect to localhost:3000;
    window.location.href = "http://localhost:3000";
    return response.data;
  }

  async function update(id, name, mail){
    const response = await axios.put(`http://localhost:8080/updateOne/${id}`, {
      name: name,
      mail: mail,
    });
    console.log('updating', id, name, mail);
    console.log(response);
    window.location.href = "http://localhost:3000";
    return response.data;
  }
  const [name, setName] = React.useState(props.name);
  const [mail, setMail] = React.useState(props.mail);
  const [edit, setEdit] = React.useState(false);
  return (
    <div className="item">
      <div className="itemInfo">
        <div className="img">👨‍🦳</div>
        <div className="text">
          {!edit ? <p>{props.name}</p> : <input type="text" value={name} placeholder={props.name} onChange={(e) => setName(e.target.value)}/>}
         {!edit ? <span>{props.mail}</span> : <input type="mail" value={mail} placeholder={props.mail} onChange={(e) => setMail(e.target.value)}/>}
         {!edit ? '' : <p className="editButton" onClick={() => update(props.id, name, mail)}>Update 📝</p>}
        </div>
      </div>
      <div className="itemInterract">
        <div className="dot"></div>
        <div className="dot"></div>
        <div className="dot"></div>
        <div className="interraction">
          <div className="delete button" onClick={() => del(props.id)}>
            <p>🗑️ Delete</p>
          </div>
          <div className="edit button" onClick={() => setEdit(!edit)}>
            <p>📝 Edit</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Elve;