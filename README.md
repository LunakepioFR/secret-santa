# SECRET SANTA 

Secret Santa is a web Application made by Quentin Aubert and Moulinneuf Alex. Powered by React.JS, Express and MySQL.
It provides a CRUD where you can make your own Secret Santa online.

## REQUIREMENTS

To initiate the project you need to have docker installed on your machine.

## GETTING STARTED

To get started launch this command inside the project folder.
````
docker-compose up -d
````

## STOP PROJECT

To stop the project launch this command inside the project folder.
````
docker-compose stop
````

## FEATURES

You can create read update and delete items in a list to manage your secret santa contacts and make a randomized peer matching.

